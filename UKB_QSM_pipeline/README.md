This directory contains the complete QSM pipeline in UK Biobank, including main QSM processing, CSF referencing, image registration and IDP extraction, etc.

* The Python scripts for mixture modeling (to calculate magentic susceptibility in CSF) were adapted from https://github.com/allera/One_Dim_Mixture_Models

* The rest of the scripts in this directory were originally written by Chaoyue Wang, adapted to UK Biobank by Fidel Alfaro-Almagro. Therefore, these scripts are included in the UK Biobank brain imaging processing pipeline https://git.fmrib.ox.ac.uk/falmagro/uk_biobank_pipeline_v_1.5
