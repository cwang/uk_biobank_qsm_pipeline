#!/bin/env python

#Add the toolbox to path
import numpy as np
import nibabel as nib
import os
import sys
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-id', dest="ID", type=str, nargs=1)
parser.add_argument('-codedir', dest="path", type=str, nargs=1)
argsa = parser.parse_args()
subID = ' '.join(map(str, argsa.ID))
codepath = ' '.join(map(str, argsa.path))

toolbox_path = (codepath +'/One_Dim_Mix_Mod')
sys.path.append(os.path.join(os.path.abspath(toolbox_path)))

#load ROI data
QSM = nib.load(subID + '/ROI_QSM_CSF.nii.gz')
CSF = np.array(QSM.dataobj)

CSF = CSF.ravel()
data = CSF [CSF != 0]

data_vector=(data - np.mean(data))/np.std(data)

#Define options for the mixture model fit
Inference ='Method of moments'#'Variational Bayes'#'Method of moments'#'Variational Bayes'  #'Method of moments' OR 'Maximum Likelihood' OR 'Variational Bayes' ML NOT INCLUDED YET
Number_of_Components=3
Components_Model=['Gauss','InvGamma','-InvGamma'] #Each component can be Gauss, Gamma, InvGamma, -Gamma, -InvGamma
init_params=[0,1,5,2,-5,2]
init_pi=np.ones(3);
init_pi=np.divide(init_pi,3)
#init_pi[0]=0.9;init_pi[1]=0.05;init_pi[2]=0.05
maxits=300
tol=0.00000001
opts={'Inference':Inference,'Number_of_Components':Number_of_Components,'Components_Model':Components_Model,
                                        'init_params':init_params,'maxits':maxits,'tol':tol,'init_pi':init_pi}
#Define options for the mixture model fit


# CALL TO FIT MIXTURE MODEL
from Mixture_Model_1Dim import Mixture_Model_1Dim     
Model = Mixture_Model_1Dim(data_vector, opts)

Means=Model['mu1'][0]* np.std(data)+np.mean(data)

f = open(subID + "/QSM_CSF.txt", "w")
f.write(str(Means))
f.close()
