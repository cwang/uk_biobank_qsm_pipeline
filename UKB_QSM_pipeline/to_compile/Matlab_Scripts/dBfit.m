function dB = dBfit(phase, TEs)

% Description: deltaB fitting for 3-echo GRE data from GE scanners
%
% Authors: Chaoyue Wang, Benjamin C. Tendler & Karla L. Miller
%
% Copyright 2021 University of Oxford
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
% http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

% phase - phase images
% TEs - echo times

[sx,sy,sz,ne] = size(phase);

phase = permute(phase,[4 1 2 3]);

phase = reshape(phase,ne,[]);

TE_rep = repmat(TEs(:),[1 sx*sy*sz]);

TE_rep=nets_demean(TE_rep,1);
phase=nets_demean(phase,1);

dB = sum(phase.*TE_rep,1)./(sum(TE_rep.*TE_rep)+eps);
dB = reshape(dB,[sx sy sz]);

end
