This directory contains MATLAB scripts to generate QSM spatial maps, without further steps such as CSF referencing, image registration or IDP extraction. If you’d like to see the complete pipeline including these extra steps, please go to the other directory `UKB_QSM_pipeline` in this repository, which includes the complete QSM pipeline as part of the UK Biobank image processing pipeline.

* `UKBiobank_QSM.m`:  This script is the main QSM processing pipeline for UK Biobank swMRI data. `phasevariance_nonlin.m` and `weightedCombination.m` (adapted from the [MCPC-3D-S](https://github.com/korbinian90/ASPIRE) algorithm) are supporting functions.

* The multi-channel phase combination algorithm MCPC-3D-S used has been implemented in Julia as well (https://github.com/korbinian90/MriResearchTools.jl).

## Requirements
- MATLAB R2017b or higher
- STI Suite - https://people.eecs.berkeley.edu/~chunlei.liu/software.html
- FSL - https://fsl.fmrib.ox.ac.uk/fsldownloads_registration
