# UK Biobank QSM pipeline

This repository includes scripts for UK Biobank QSM processing, used in Wang et al., 2022, [Phenotypic and genetic associations of quantitative magnetic susceptibility in UK Biobank brain imaging,](https://www.nature.com/articles/s41593-022-01074-w) _Nature Neuroscience_. (https://doi.org/10.1038/s41593-022-01074-w)

* `QSM_only_scripts_MATLAB` contains MATLAB scripts to generate QSM spatial maps.

* `UKB_QSM_pipeline` contains the complete QSM pipeline in UK Biobank, including QSM processing, CSF referencing, image registration and IDP extraction, etc., which is part of the UK Biobank brain imaging processing pipeline https://git.fmrib.ox.ac.uk/falmagro/uk_biobank_pipeline_v_1.5.

* Contributors: Chaoyue Wang, Fidel Alfaro-Almagro, Alberto Llera, Stephen M. Smith, Benjamin C. Tendler and Karla L. Miller

## Requirements
- MATLAB R2017b or higher
- STI Suite - https://people.eecs.berkeley.edu/~chunlei.liu/software.html
- FSL - https://fsl.fmrib.ox.ac.uk/fsldownloads_registration
- Python 2.7
